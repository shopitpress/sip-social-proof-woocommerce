<div class="card-deck mb-3 text-center">
        <div class="card mb-4">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal ">SIP Social Proof</h4>
            <h1 class="card-title pricing-card-title ">$0</h1>
         <small class="text-muted pricing"> Free forever</small>
          </div>
          <ul class="list-group pricing-group list-group-flush mt-3 mb-4">
              <li class="list-group-item pricing-group">Basic support</li>
              <li class="list-group-item pricing-group">Display <a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_sales/" target="_blank" data-toggle="tooltip" title="" data-original-title="Displays total sales of a WooCommerce product">total sales</a> in shop pages and single product pages</li>
              <li class="list-group-item pricing-group">Display <a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_sales/" target="_blank" data-toggle="tooltip" title="" data-original-title="Displays total sales of a WooCommerce product">total sales</a> of any product with a shortcode</li>
              <li class="list-group-item pricing-group">WYSIWYG styling</li>
            </ul>
          <div class="card-body">
          <button type="button" class="btn btn-lg btn-light btn-block" disabled>Active</button>
            </div>
        </div>
        <div class="card mb-4">
          <div class="card-header tx-white bg-danger">
            <h4 class="my-0 font-weight-normal tx-white">SIP Social Proof Add-on</h4>
            <h1 class="card-title pricing-card-title tx-white">$19.00 <small class="sale-price-text">$25</small></h1>
           <small class="text-muted tx-white pricing">50% OFF Limited Time Offer</small>
          </div>
          <ul class="list-group pricing-group list-group-flush mt-3 mb-4">
              <li class="list-group-item pricing-group">Priority support</li>
              <li class="list-group-item pricing-group">Display advanced sales and customers stats in shop pages and single product pages</li>
              <li class="list-group-item pricing-group">Display advanced sales and customers stats with a shortcode</li>
              <li class="list-group-item pricing-group"><a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/#Shortcodes_and_variables_available_for_theAdd-on" data-toggle="tooltip" title="" data-original-title="Total sales, Daily sales, Weekly sales, Monthly sales, and Time since last sale.">Advanced sales stats</a></li>
            <li class="list-group-item pricing-group"><a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/#Shortcodes_and_variables_available_for_theAdd-on" target="_blank" data-toggle="tooltip" title="" data-original-title="Number of units sold to a customer, Number of sales a customer made, and Number of customers who bought a product.">Advanced stats per customer</a></li>
            <li class="list-group-item pricing-group"><a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/#Shortcodes_and_variables_available_for_theAdd-on" target="_blank" data-toggle="tooltip" title="" data-original-title="Number of customers of the store, Number of orders of the store, and Number of units sold in the store.">Advanced store stats</a></li>
            <li class="list-group-item pricing-group"><a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/#Shortcodes_and_variables_available_for_theAdd-on" target="_blank" data-toggle="tooltip" title="" data-original-title="Number of people looking at your product or page">Advanced visitor stats</a></li>
            <li class="list-group-item pricing-group"><a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/#Shortcodes_and_variables_available_for_theAdd-on" target="_blank" data-toggle="tooltip" title="" data-original-title="Multiply the result, Hide under a certain value, Show a certain number under a value, and Choose random number.">Advanced variables</a></li>
             <li class="list-group-item pricing-group">WYSIWYG styling</li>
            </ul>
          <div class="card-body">
            
             <button type="button" class="btn btn-lg btn-danger btn-block">Get started with 50% off</button>
           
          </div>

        </div>
      </div>

      <!---old plugin code-->
<!-- <div class="get-it-now-col">
	<div class="get-it-now-col-left">
    	<span class="getitnoe-hed-left">SIP Social Proof</span>
        <table class="sip-soc-proff-table" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="td-height font-styl">Basic support</td>
            </tr>
            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Display <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/free/total_sales/">total sales</a> in shop pages and single product pages</td>
            </tr>
            <tr>
                <td class="td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Display <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/free/total_sales/">total sales</a> of any product with a&nbsp;shortcode</td>
            </tr>
            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i>&nbsp;WYSIWYG styling</td>
            </tr>
        </table>
    </div>
	<div class="get-it-now-col-left padd--shdw-right">
    	<div class="right-getitnow-hed">
            <span class="getitnoe-hed-right">SIP Social Proof Add-on</span>
            <span class="getitnoe-subhed">50% OFF - Limited time</span>
        </div>
        <table class="sip-soc-proff-table" cellpadding="0" cellspacing="0" border="0">
            <tr>
            </tr>
            <tr>
                <td class="white-bg td-height font-styl">Priority support</td>
            </tr>
            <tr>
                <td class="td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Display advanced sales and customers stats&nbsp;in shop pages and single product pages</td>
            </tr>
            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Display advanced sales and customers stats&nbsp;with a&nbsp;shortcode</td>
            </tr>
            <tr>
                <td class="td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Advanced sales stats:<br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_sales/">total sales</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/daily_sales/">daily sales</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/weekly_sales/">weekly sales</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/monthly_sales/">monthly sales</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/hours-and-minutes/">time since last sale</a>
                </td>
            </tr>

            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Advanced stats per customer:<br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customer_units/">number of units sold to a customer</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customer_sales/">number of sales a customer made</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/unique_product_customers/">number of customers who bought a product</a>
                </td>
            </tr>

            <tr>
                <td class="td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Advanced store&nbsp;stats:<br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_unique_product_customers/">number of customers of the store</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/store_sales/">number of orders of the store</a><br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/store_units/">number of units sold in the store</a>
                </td>
            </tr>

            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Advanced visitor&nbsp;stats:<br>
                    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customers_looking/">number of people looking at your product/page </a>
                </td>
            </tr>

            <tr>
                <td class="td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i> Advanced variables:<br>
                    multiply the result<br>
                    hide under a certain value<br>
                    show a certain number under a value<br>
                    choose random number
                </td>
            </tr>

            <tr>
                <td class="white-bg td-height font-styl"><i class="icon-tiny fa-check-circle accent-color"></i>&nbsp;WYSIWYG styling</td>
            </tr>
            <tr>
                <td class="td-height font-styl">
                    <div class="button_div">
                  	    <a class="button" target="_blank" href="https://shopitpress.com/plugins/sip-social-proof-woocommerce/?utm_source=wordpress.org&amp;utm_medium=banner&amp;utm_content=<?php echo SIP_SPWC_ADMIN_VERSION; ?>&amp;utm_campaign=<?php echo SIP_SPWC_UTM_CAMPAIGN; ?>">Learn More</a>
                  	</div>
                </td>
            </tr>
        </table>
    </div>
    <div class="clearfix"></div>
</div> -->