<?php 

if (isset($_POST['reset-stats-settings'])) {
    
    global $wpdb;

    $count = $wpdb->query(
        "DELETE FROM $wpdb->options
        WHERE option_name LIKE '\_transient_wsp\_%'"
    );

} 

?>
<form method="post" action="options.php">
    <?php settings_fields( 'sip-spwc-settings-group' ); ?>
	<div class="row">
				
<div class="col-md-6 col-xl-6">
				
<div class="card ">

<div class="card-header">
<div class="custom-control custom-checkbox">
 <input class="custom-control-input" type="checkbox" id="enable_in_single_product_page" name="display_sales_single_product_page" value="true" <?php echo esc_attr( get_option('display_sales_single_product_page', false))?' checked="checked"':''; ?> />
 <label class="custom-control-label" for="enable_in_single_product_page"> Enable in Single Product Page </label>
</div>

</div>



<div class="card-body">
 

 <?php 
                    $settings       = array('teeny' => false, 'tinymce' => true, 'textarea_rows' => 12, 'tabindex' => 1 );
                    $editor_id      = "show_in_product_page_view_editor"; 
                    $editor_content = get_option('show_in_product_page_view_editor'); 
                    wp_editor( $editor_content, $editor_id, $settings );
                ?>

</div>
 
</div>
</div>
<div class="col-md-6 col-xl-6">
<div class="card ">
<div class="card-header">
<div class="custom-control custom-checkbox">
<input class="custom-control-input" type="checkbox" id="enable_in_shop_page" name="display_sales_shop_page" value="true" <?php echo esc_attr( get_option('display_sales_shop_page', false))?' checked="checked"':''; ?> />
<label class="custom-control-label" for="enable_in_shop_page"> Enable in Shop Page</label>
</div>
</div>

<div class="card-body">
 
 <?php 
                    $settings       = array('teeny' => false, 'tinymce' => true, 'textarea_rows' => 12, 'tabindex' => 1 );
                    $editor_id      = "show_in_product_in_list_view_editor"; 
                    $editor_content = get_option('show_in_product_in_list_view_editor'); 
                    wp_editor( $editor_content, $editor_id, $settings );
                ?>
</div>
</div>
</div>

<div class="col-md-12 col-xl-12 mg-t-20">
<div class="card ">

<div class="card-footer text-center border-top-0">
<input type="submit" name="submit" id="submit" class="btn btn-danger" value="Save Changes">
</div>
</div>
</div>
</div>



   <?php 
        //submit_button(); 
    ?>
</form>

<?php if( class_exists( 'SIP_Social_Proof_Add_On_WC' ) ) { ?>
    <form method="post" action="">
        <?php 
            submit_button( 'Reset Stats', 'secondary' , 'reset-stats-settings');
        ?>
    </form>
    <form method="post" action="options.php">
        <?php settings_fields( 'sip-spwc-hours-settings-group' ); ?>
        <table class="form-table">
          <tr valign="top">
            <td><label>Refresh stats every <input type="number" name="refresh_stats_every_x_hours" value="<?php echo get_option('refresh_stats_every_x_hours'); ?>" style="width: 50px;" /> hours</label></td>
          </tr> 
        </table>
		<input type="submit" name="submit" id="submit" class="btn btn-danger" value="Save Changes">
        <?php 
          //  submit_button(); 
        ?>
    </form>
<?php } ?>