<div class="row">

<div class="col-md-6 col-xl-6 mg-b-20 mg-t-10">
<div class="card ht-100p">

<div class="card-header"> <h5 class="card-title">Usage</h5></div>
 <div class="card-body">
    <p>For a detailed explanation on usage and examples please visit the <a class="sip-link" href="https://shopitpress.com/docs/sip-social-proof-woocommerce/?utm_source=wordpress.org&amp;utm_medium=help&amp;utm_content=v<?php echo SIP_SPWC_PLUGIN_VERSION ?>&amp;utm_campaign=sip_social_proof" target="_blank">SIP Social Proof documentation</a>.</p>

  </div>		
			
	</div>		
           
          

</div>
<div class="col-md-6 col-xl-6 mg-b-20 mg-t-10">
<div class="card mg-b-20 ht-100p">

<div class="card-header"> <h5 class="card-title">Questions and support</h5></div>
 <div class="card-body">
  <p>All of our plugins come with free support. We care about your plugin after purchase just as much as you do.</p>
        <p>We want to make your life easier and make you happy about choosing our plugins. We guarantee to respond to every inquiry within 1 business day.
        Please visit our <a class="sip-link" href="<?php echo SIP_SPWC_PLUGIN_PURCHASE_URL; ?>?utm_source=wordpress.org&amp;utm_medium=help&amp;utm_content=v<?php echo SIP_SPWC_PLUGIN_VERSION ?>&amp;utm_campaign=sip_social_proof" target="_blank">community</a> and ask us anything you need.</p>
    
  </div>    
      
  </div>  
           
        

		

</div>

<div class="col-md-12 col-xl-12">

<div class="card">
<div class="card-footer text-center border-top-0">

<div class="sip-versionm">
 <?php $get_optio_version = get_option( 'sip_version_value' ); echo "SIP Version " . $get_optio_version; ?>
 </div> 
<!-- .sip-version -->
</div>
  
  </div>
  
</div>



</div>



