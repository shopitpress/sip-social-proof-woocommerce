<div class="row">		
<div class="col-md-6 col-xl-6">		
<div class="card ">
<div class="card-header"> SIP Social Proof</div>

<div class="card-body">

<ul class="list-group">
  <li class="list-group-item">Basic support</li>
  <li class="list-group-item"> Display <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/free/total_sales/">total sales</a> in shop pages and single product pages</li>
  <li class="list-group-item">Display <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/free/total_sales/">total sales</a> of any product with a&nbsp;shortcode</li>
   <li class="list-group-item">&nbsp;WYSIWYG styling</li>
  <li class="list-group-item"></li>
  <li class="list-group-item"></li>
	
	
</ul>

</div>

</div>
</div>


<div class="col-md-6 col-xl-6">
<div class="card ">
<div class="card-header"> SIP Social Proof Add-on <span>50% OFF - Limited time</span></div>
<div class="card-body">
<ul class="list-group">
 
  <li class="list-group-item">Priority support</li>
  <li class="list-group-item">Display advanced sales and customers stats&nbsp;in shop pages and single product pages</li>
  <li class="list-group-item">Display advanced sales and customers stats&nbsp;with a&nbsp;shortcode</li>
  
  
  <li class="list-group-item"> Advanced sales stats:</li>
   
   <li class="list-group-item"> <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_sales/">total sales</a><br>
                   </li>
				   <li class="list-group-item"> <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/daily_sales/">daily sales</a><br>
                  </li> <li class="list-group-item"> <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/weekly_sales/">weekly sales</a><br>
                  </li> <li class="list-group-item"> <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/monthly_sales/">monthly sales</a><br>
                  </li><li class="list-group-item">  <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/hours-and-minutes/">time since last sale</a>
					
					
				</li><li class="list-group-item">	 Advanced stats per customer:
					
					
				</li><li class="list-group-item">	 <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customer_units/">number of units sold to a customer</a><br>
                </li> <li class="list-group-item">   <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customer_sales/">number of sales a customer made</a><br>
                </li>  <li class="list-group-item">  <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/unique_product_customers/">number of customers who bought a product</a>
                
				</li><li class="list-group-item">Advanced store&nbsp;stats:
				
				
				
				
				
				</li><li class="list-group-item"><a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/total_unique_product_customers/">number of customers of the store</a><br>
                </li><li class="list-group-item">    <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/store_sales/">number of orders of the store</a><br>
                </li> <li class="list-group-item">   <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/store_units/">number of units sold in the store</a>
                
				
				
				</li><li class="list-group-item">Advanced visitor&nbsp;stats:
				
				 </li><li class="list-group-item"> <a href="https://shopitpress.com/docs/sip-social-proof-woocommerce/add-on/customers_looking/">number of people looking at your product/page </a>
               
				
				</li>	<li class="list-group-item">Advanced variables:<br>
                  </li> <li class="list-group-item"> multiply the result<br>
                  </li> <li class="list-group-item"> hide under a certain value<br>
                  </li> <li class="list-group-item"> show a certain number under a value<br>
                    </li> <li class="list-group-item">choose random number
					
					
					
					
					</li> <li class="list-group-item">&nbsp;WYSIWYG styling
					
					
					
					</li>
					<li class="list-group-item"> <a class="btn btn-primary" target="_blank" href="https://shopitpress.com/plugins/sip-social-proof-woocommerce/?utm_source=wordpress.org&amp;utm_medium=banner&amp;utm_content=<?php echo SIP_SPWC_ADMIN_VERSION; ?>&amp;utm_campaign=<?php echo SIP_SPWC_UTM_CAMPAIGN; ?>">Learn More</a>
                  	
					
					
					
  </li>
 
</ul>
</div>
</div>
</div>

</div>
